package main

import (
	"database/sql"
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"

	_ "github.com/mattn/go-sqlite3"
)

var DB *sql.DB

func init() {

	os.Remove("db")
	file, _ := os.Create("db")
	file.Close()
}

func main() {

	fmt.Println("server started")

	var err error
	DB, err = sql.Open("sqlite3", "./db")
	if err != nil {
		log.Fatal(err)
	}
	defer DB.Close()

	http.HandleFunc("/", handleHttp)
	log.Fatal(http.ListenAndServe("localhost:8000", nil))
}

func sqlAddCar(db *sql.DB, car string) {

	createCarTableSQL := `CREATE TABLE IF NOT EXISTS "` + car + `" ("part" TEXT);`

	fmt.Println(createCarTableSQL)

	statement, err := db.Prepare(createCarTableSQL)
	if err != nil {
		log.Fatal(err)
	}
	statement.Exec()
}

func sqlAddPart(db *sql.DB, car, part string) {

	addPartSQL := `INSERT INTO "` + car + `" VALUES ("` + part + `");`
	fmt.Println(addPartSQL)

	statement, err := db.Prepare(addPartSQL)
	if err != nil {
		log.Fatal(err)
	}
	statement.Exec()
}

func sqlGetParts(db *sql.DB, car string) []string {

	parts := make([]string, 0)

	getPartsSQL := `SELECT * FROM "` + car + `";`
	fmt.Println(getPartsSQL)

	row, err := db.Query(getPartsSQL)
	if err != nil {
		log.Fatal(err)
	}

	for row.Next() {
		var part string
		row.Scan(&part)
		parts = append(parts, part)
	}

	return parts
}

func handleHttp(w http.ResponseWriter, r *http.Request) {

	fmt.Println(r.Method + " " + r.URL.Path)

	tokens := make([]string, 2)
	tokens = strings.Split(r.URL.Path, "/")

	switch r.Method {
	case "POST":
		handleCreate(tokens[1], tokens[2])
	case "GET":
		parts := handleRead(tokens[1])
		fmt.Fprintln(w, parts)
	}
}

func handleCreate(car, part string) {

	if len(car) != 0 && len(part) != 0 {
		sqlAddCar(DB, car)
		sqlAddPart(DB, car, part)
	}
}

func handleRead(car string) string {

	ret := ""

	if len(car) != 0 {
		sqlAddCar(DB, car)
		parts := sqlGetParts(DB, car)
		for _, part := range parts {
			ret += part + "\n"
		}
	}

	return ret
}
