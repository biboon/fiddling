# Fiddling

This repository contains various snippets for just practicing.

## Todo

* Add a logger
	https://github.com/sirupsen/logrus
	https://github.com/uber-go/zap
	https://github.com/rs/zerolog
* JSON requests and responses
* REST API format
	* /car
		* GET -> lister toutes les bagnoles
		* POST -> creer une bagnole
	* /car/{car-id}
		* GET -> donne les details d'une caisse
		* DELETE -> delete une caisse
		* PUT/PATCH -> modifier une caisse
	* /car/{car-id}/part
		* GET -> lister toutes les pieces d'une vroum vroum
		* POST -> creer ...
	* /car/{car-id}/part/{part-id}
		* GET -> recup piece
		* DELETE -> drop piece
		* PUT/PATCH -> modififer une piece

* Des objets putain (aka marshal/unmarshal)
	* car
		* color
		* production_date
		* service_led
		* last_odometer
	* part
		* car-id
		* name
		* install_date
		* price

PUT/PATCH
	* PUT: pour modifier le contenu (avec toutes les donnees)
	* PATCH: pour modifier le contenu (avec uniquement les champs a modifier)

* A real DB, I mean PostgreSQL

* Use a HTTP API library
	* https://github.com/gorilla/mux

* Test EVERYTHING

* Env var for everything

* Create code architecturea
	For reference: https://github.com/golang-standards/project-layout#go-directories
	* cmd/{app-name}/main.go -- balek
	* pkg/ <-- balek
	* internal/ <-- c'est la
		* datastore/
		* model/
			* car.go
			* part.go
		* http-server/
		* controller/

	Pour aller plus loin:
	* api/ (swagger)

For reference:

client -> HTTP -> controller -> datastore -> DB

* Add CI to check test at every commit
